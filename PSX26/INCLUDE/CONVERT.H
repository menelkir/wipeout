/*
 * File:convert.h
 */
extern unsigned int todigit();
extern double atof();
extern int atoi();
extern long atol();
extern double strtod();
extern long strtol();
extern unsigned long strtoul();
extern long labs();
/*
extern int abs();
extern div_t div();
extern ldiv_t ldiv();
*/


