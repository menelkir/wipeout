#define	DEBUG_LOCKED	0
extern char *DebugScreen;

void setupScreens(void);
void DDSwap(void);
void WinClearScreen(char colour);
void CloseATI3DCIF (void);
void ShellMovieFinished(void);
void mpeganim(void);
void ShowErrorMessage(UINT StrNum);
