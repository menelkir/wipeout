int sar(int a,int n);
int GetMagnitude(VECTOR vec1);
void MinusVector(VECTOR v1, VECTOR v2, VECTOR *result);
void CrossProduct(VECTOR *result, VECTOR A, VECTOR B);
int ScalarProduct(VECTOR vec1, VECTOR vec2);
int GetAng(VECTOR vec1, VECTOR vec2);
int ang(int a);
int arccos(int value);
