void ClipPolyF4(P_TAG* ordt, POLY_F4 *prim);
void ClipPolyF3(P_TAG* ordt, POLY_F3 *prim);
void ClipPolyG4(P_TAG* ordt, POLY_G4 *prim);
void ClipPolyG3(P_TAG* ordt, POLY_G3 *prim);
void ClipPolyFT4(P_TAG* ordt, POLY_FT4 *prim);
void ClipPolyFT3(P_TAG* ordt, POLY_FT3 *prim);
void ClipPolyGT4(P_TAG* ordt, POLY_GT4 *prim);
void ClipPolyGT3(P_TAG* ordt, POLY_GT3 *prim);
