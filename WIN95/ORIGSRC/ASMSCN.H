
/* (C) Psygnosis 1994. By Jason Carl Denton
*/

short AsmDrawLibObj
( 
   Object* object, 
   Skeleton* camera 
);
