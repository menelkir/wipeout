Ambient light color: Red=0.3 Green=0.3 Blue=0.3

Named object: "NoName1"
Tri-mesh, Vertices: 62     Faces: 124
Vertex list:
Vertex 0:  X:-0.186068     Y:-0.268000     Z:0.100000
Vertex 1:  X:-0.186068     Y:0.268865     Z:0.100000
Vertex 2:  X:0.042081     Y:0.268865     Z:0.100000
Vertex 3:  X:0.167142     Y:0.254400     Z:0.100000
Vertex 4:  X:0.202252     Y:0.234304     Z:0.100000
Vertex 5:  X:0.229581     Y:0.202947     Z:0.100000
Vertex 6:  X:0.247159     Y:0.163305     Z:0.100000
Vertex 7:  X:0.253018     Y:0.118352     Z:0.100000
Vertex 8:  X:0.244138     Y:0.063192     Z:0.100000
Vertex 9:  X:0.217496     Y:0.018560     Z:0.100000
Vertex 10:  X:0.173184     Y:-0.013529     Z:0.100000
Vertex 11:  X:0.111295     Y:-0.031062     Z:0.100000
Vertex 12:  X:0.169339     Y:-0.076106     Z:0.100000
Vertex 13:  X:0.231046     Y:-0.163264     Z:0.100000
Vertex 14:  X:0.296598     Y:-0.268000     Z:0.100000
Vertex 15:  X:0.166959     Y:-0.268000     Z:0.100000
Vertex 16:  X:0.088590     Y:-0.151179     Z:0.100000
Vertex 17:  X:0.031461     Y:-0.072260     Z:0.100000
Vertex 18:  X:-0.001132     Y:-0.049921     Z:0.100000
Vertex 19:  X:-0.055697     Y:-0.043879     Z:0.100000
Vertex 20:  X:-0.077670     Y:-0.043879     Z:0.100000
Vertex 21:  X:-0.077670     Y:-0.268000     Z:0.100000
Vertex 22:  X:-0.077670     Y:0.041814     Z:0.100000
Vertex 23:  X:0.002530     Y:0.041814     Z:0.100000
Vertex 24:  X:0.099942     Y:0.048406     Z:0.100000
Vertex 25:  X:0.130338     Y:0.071111     Z:0.100000
Vertex 26:  X:0.141324     Y:0.111394     Z:0.100000
Vertex 27:  X:0.126859     Y:0.155157     Z:0.100000
Vertex 28:  X:0.086026     Y:0.176214     Z:0.100000
Vertex 29:  X:0.006925     Y:0.178045     Z:0.100000
Vertex 30:  X:-0.077670     Y:0.178045     Z:0.100000
Vertex 31:  X:-0.186068     Y:-0.268000     Z:-0.100000
Vertex 32:  X:-0.077670     Y:-0.268000     Z:-0.100000
Vertex 33:  X:-0.077670     Y:-0.043879     Z:-0.100000
Vertex 34:  X:-0.055697     Y:-0.043879     Z:-0.100000
Vertex 35:  X:-0.001132     Y:-0.049921     Z:-0.100000
Vertex 36:  X:0.031461     Y:-0.072260     Z:-0.100000
Vertex 37:  X:0.088590     Y:-0.151179     Z:-0.100000
Vertex 38:  X:0.166959     Y:-0.268000     Z:-0.100000
Vertex 39:  X:0.296598     Y:-0.268000     Z:-0.100000
Vertex 40:  X:0.231046     Y:-0.163264     Z:-0.100000
Vertex 41:  X:0.169339     Y:-0.076105     Z:-0.100000
Vertex 42:  X:0.111295     Y:-0.031062     Z:-0.100000
Vertex 43:  X:0.173185     Y:-0.013529     Z:-0.100000
Vertex 44:  X:0.217496     Y:0.018560     Z:-0.100000
Vertex 45:  X:0.244138     Y:0.063192     Z:-0.100000
Vertex 46:  X:0.253019     Y:0.118353     Z:-0.100000
Vertex 47:  X:0.247159     Y:0.163305     Z:-0.100000
Vertex 48:  X:0.229581     Y:0.202947     Z:-0.100000
Vertex 49:  X:0.202253     Y:0.234304     Z:-0.100000
Vertex 50:  X:0.167142     Y:0.254400     Z:-0.100000
Vertex 51:  X:0.042081     Y:0.268865     Z:-0.100000
Vertex 52:  X:-0.186068     Y:0.268865     Z:-0.100000
Vertex 53:  X:-0.077670     Y:0.041814     Z:-0.100000
Vertex 54:  X:-0.077670     Y:0.178045     Z:-0.100000
Vertex 55:  X:0.006925     Y:0.178045     Z:-0.100000

                                     Page 1



Vertex 56:  X:0.086026     Y:0.176214     Z:-0.100000
Vertex 57:  X:0.126859     Y:0.155157     Z:-0.100000
Vertex 58:  X:0.141324     Y:0.111394     Z:-0.100000
Vertex 59:  X:0.130338     Y:0.071111     Z:-0.100000
Vertex 60:  X:0.099942     Y:0.048406     Z:-0.100000
Vertex 61:  X:0.002530     Y:0.041814     Z:-0.100000
Face list:
Face 0:    A:11 B:20 C:19 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 1:    A:11 B:19 C:18 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 2:    A:11 B:18 C:17 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 3:    A:11 B:17 C:12 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 4:    A:12 B:17 C:16 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 5:    A:12 B:16 C:13 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 6:    A:13 B:16 C:15 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 7:    A:13 B:15 C:14 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 8:    A:2 B:1 C:3 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 9:    A:3 B:1 C:4 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 10:    A:4 B:1 C:5 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 11:    A:5 B:1 C:30 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 12:    A:30 B:1 C:22 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 13:    A:23 B:22 C:9 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 14:    A:9 B:22 C:10 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 15:    A:10 B:22 C:11 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 16:    A:11 B:22 C:20 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 17:    A:22 B:1 C:20 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1

                                     Page 2



Face 18:    A:20 B:1 C:0 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 19:    A:20 B:0 C:21 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 20:    A:5 B:30 C:29 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 21:    A:5 B:29 C:28 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 22:    A:5 B:28 C:6 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 23:    A:6 B:28 C:27 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 24:    A:6 B:27 C:7 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 25:    A:7 B:27 C:26 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 26:    A:7 B:26 C:25 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 27:    A:7 B:25 C:8 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 28:    A:8 B:25 C:24 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 29:    A:8 B:24 C:9 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 30:    A:24 B:23 C:9 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 31:    A:32 B:21 C:31 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 32:    A:31 B:21 C:0 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 33:    A:30 B:22 C:53 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 34:    A:30 B:53 C:54 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 35:    A:20 B:21 C:32 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 36:    A:20 B:32 C:33 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 37:    A:34 B:19 C:33 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 38:    A:33 B:19 C:20 AB:1 BC:1 CA:1

                                     Page 3



Material:"r210g210b210a0"
Smoothing:  1
Face 39:    A:35 B:18 C:34 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 40:    A:34 B:18 C:19 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 41:    A:36 B:17 C:35 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 42:    A:35 B:17 C:18 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 43:    A:16 B:17 C:36 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 44:    A:16 B:36 C:37 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 45:    A:15 B:16 C:37 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 46:    A:15 B:37 C:38 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 47:    A:39 B:14 C:38 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 48:    A:38 B:14 C:15 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 49:    A:13 B:14 C:39 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 50:    A:13 B:39 C:40 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 51:    A:12 B:13 C:40 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 52:    A:12 B:40 C:41 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 53:    A:12 B:41 C:42 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 54:    A:12 B:42 C:11 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 55:    A:43 B:10 C:42 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 56:    A:42 B:10 C:11 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 57:    A:44 B:9 C:43 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 58:    A:43 B:9 C:10 AB:1 BC:1 CA:1
Material:"r210g210b210a0"

                                     Page 4



Smoothing:  1
Face 59:    A:8 B:9 C:44 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 60:    A:8 B:44 C:45 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 61:    A:7 B:8 C:45 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 62:    A:7 B:45 C:46 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 63:    A:6 B:7 C:46 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 64:    A:6 B:46 C:47 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 65:    A:5 B:6 C:47 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 66:    A:5 B:47 C:48 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 67:    A:4 B:5 C:48 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 68:    A:4 B:48 C:49 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 69:    A:4 B:49 C:50 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 70:    A:4 B:50 C:3 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 71:    A:3 B:50 C:51 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 72:    A:3 B:51 C:2 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 73:    A:2 B:51 C:52 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 74:    A:2 B:52 C:1 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 75:    A:0 B:1 C:52 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 76:    A:0 B:52 C:31 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 77:    A:34 B:42 C:35 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 78:    A:35 B:42 C:36 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1

                                     Page 5



Face 79:    A:36 B:42 C:41 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 80:    A:36 B:41 C:37 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 81:    A:37 B:41 C:40 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 82:    A:37 B:40 C:39 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 83:    A:37 B:39 C:38 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 84:    A:55 B:48 C:56 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 85:    A:56 B:48 C:47 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 86:    A:56 B:47 C:57 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 87:    A:57 B:47 C:46 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 88:    A:57 B:46 C:58 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 89:    A:58 B:46 C:59 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 90:    A:59 B:46 C:45 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 91:    A:59 B:45 C:60 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 92:    A:60 B:45 C:44 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 93:    A:61 B:60 C:44 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 94:    A:53 B:61 C:44 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 95:    A:52 B:51 C:50 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 96:    A:52 B:50 C:49 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 97:    A:52 B:49 C:48 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 98:    A:52 B:48 C:55 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 99:    A:52 B:55 C:54 AB:1 BC:1 CA:1

                                     Page 6



Material:"r210g210b210a0"
Smoothing:  1
Face 100:    A:52 B:54 C:53 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 101:    A:53 B:44 C:43 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 102:    A:53 B:43 C:42 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 103:    A:53 B:42 C:34 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 104:    A:52 B:53 C:34 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 105:    A:52 B:34 C:33 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 106:    A:52 B:33 C:32 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 107:    A:52 B:32 C:31 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 108:    A:55 B:29 C:54 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 109:    A:54 B:29 C:30 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 110:    A:56 B:28 C:55 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 111:    A:55 B:28 C:29 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 112:    A:57 B:27 C:56 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 113:    A:56 B:27 C:28 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 114:    A:26 B:27 C:57 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 115:    A:26 B:57 C:58 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 116:    A:25 B:26 C:58 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 117:    A:25 B:58 C:59 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 118:    A:25 B:59 C:60 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 119:    A:25 B:60 C:24 AB:1 BC:1 CA:1
Material:"r210g210b210a0"

                                     Page 7



Smoothing:  1
Face 120:    A:24 B:60 C:61 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 121:    A:24 B:61 C:23 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 122:    A:23 B:61 C:53 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 123:    A:23 B:53 C:22 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1

















































                                     Page 8



