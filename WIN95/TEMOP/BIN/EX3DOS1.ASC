Ambient light color: Red=0.3 Green=0.3 Blue=0.3

Named object: "NoName1"
Tri-mesh, Vertices: 54     Faces: 108
Vertex list:
Vertex 0:  X:-0.170801     Y:0.267865     Z:0.100000
Vertex 1:  X:0.027319     Y:0.267865     Z:0.100000
Vertex 2:  X:0.129492     Y:0.257611     Z:0.100000
Vertex 3:  X:0.173346     Y:0.238294     Z:0.100000
Vertex 4:  X:0.210425     Y:0.208173     Z:0.100000
Vertex 5:  X:0.240088     Y:0.168668     Z:0.100000
Vertex 6:  X:0.261694     Y:0.121198     Z:0.100000
Vertex 7:  X:0.274878     Y:0.063748     Z:0.100000
Vertex 8:  X:0.279273     Y:-0.005694     Z:0.100000
Vertex 9:  X:0.262793     Y:-0.119952     Z:0.100000
Vertex 10:  X:0.238348     Y:-0.172961     Z:0.100000
Vertex 11:  X:0.205298     Y:-0.214801     Z:0.100000
Vertex 12:  X:0.172156     Y:-0.239245     Z:0.100000
Vertex 13:  X:0.129126     Y:-0.257647     Z:0.100000
Vertex 14:  X:0.033179     Y:-0.269000     Z:0.100000
Vertex 15:  X:-0.170801     Y:-0.269000     Z:0.100000
Vertex 16:  X:-0.062402     Y:0.177045     Z:0.100000
Vertex 17:  X:-0.062402     Y:-0.178546     Z:0.100000
Vertex 18:  X:0.018530     Y:-0.178546     Z:0.100000
Vertex 19:  X:0.084082     Y:-0.173419     Z:0.100000
Vertex 20:  X:0.127844     Y:-0.151080     Z:0.100000
Vertex 21:  X:0.156226     Y:-0.099261     Z:0.100000
Vertex 22:  X:0.167212     Y:-0.000934     Z:0.100000
Vertex 23:  X:0.156226     Y:0.094647     Z:0.100000
Vertex 24:  X:0.125464     Y:0.146649     Z:0.100000
Vertex 25:  X:0.075293     Y:0.171918     Z:0.100000
Vertex 26:  X:-0.013696     Y:0.177045     Z:0.100000
Vertex 27:  X:-0.170801     Y:0.267865     Z:-0.100000
Vertex 28:  X:-0.170801     Y:-0.269000     Z:-0.100000
Vertex 29:  X:0.033179     Y:-0.269000     Z:-0.100000
Vertex 30:  X:0.129126     Y:-0.257647     Z:-0.100000
Vertex 31:  X:0.172156     Y:-0.239245     Z:-0.100000
Vertex 32:  X:0.205298     Y:-0.214801     Z:-0.100000
Vertex 33:  X:0.238348     Y:-0.172961     Z:-0.100000
Vertex 34:  X:0.262793     Y:-0.119952     Z:-0.100000
Vertex 35:  X:0.279272     Y:-0.005694     Z:-0.100000
Vertex 36:  X:0.274878     Y:0.063748     Z:-0.100000
Vertex 37:  X:0.261694     Y:0.121198     Z:-0.100000
Vertex 38:  X:0.240088     Y:0.168668     Z:-0.100000
Vertex 39:  X:0.210425     Y:0.208173     Z:-0.100000
Vertex 40:  X:0.173346     Y:0.238294     Z:-0.100000
Vertex 41:  X:0.129492     Y:0.257611     Z:-0.100000
Vertex 42:  X:0.027319     Y:0.267865     Z:-0.100000
Vertex 43:  X:-0.062402     Y:0.177045     Z:-0.100000
Vertex 44:  X:-0.013696     Y:0.177045     Z:-0.100000
Vertex 45:  X:0.075293     Y:0.171918     Z:-0.100000
Vertex 46:  X:0.125464     Y:0.146649     Z:-0.100000
Vertex 47:  X:0.156226     Y:0.094647     Z:-0.100000
Vertex 48:  X:0.167212     Y:-0.000934     Z:-0.100000
Vertex 49:  X:0.156226     Y:-0.099261     Z:-0.100000
Vertex 50:  X:0.127844     Y:-0.151080     Z:-0.100000
Vertex 51:  X:0.084082     Y:-0.173419     Z:-0.100000
Vertex 52:  X:0.018530     Y:-0.178546     Z:-0.100000
Vertex 53:  X:-0.062402     Y:-0.178546     Z:-0.100000
Face list:
Face 0:    A:1 B:0 C:2 AB:1 BC:1 CA:1

                                     Page 1



Material:"r210g210b210a0"
Smoothing:  1
Face 1:    A:2 B:0 C:3 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 2:    A:3 B:0 C:4 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 3:    A:4 B:0 C:16 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 4:    A:16 B:0 C:17 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 5:    A:18 B:17 C:11 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 6:    A:11 B:17 C:12 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 7:    A:12 B:17 C:13 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 8:    A:17 B:0 C:15 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 9:    A:13 B:17 C:15 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 10:    A:13 B:15 C:14 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 11:    A:4 B:16 C:26 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 12:    A:4 B:26 C:25 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 13:    A:4 B:25 C:5 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 14:    A:5 B:25 C:24 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 15:    A:5 B:24 C:6 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 16:    A:6 B:24 C:23 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 17:    A:6 B:23 C:7 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 18:    A:7 B:23 C:22 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 19:    A:7 B:22 C:8 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 20:    A:8 B:22 C:21 AB:1 BC:1 CA:1
Material:"r210g210b210a0"

                                     Page 2



Smoothing:  1
Face 21:    A:8 B:21 C:9 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 22:    A:9 B:21 C:20 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 23:    A:9 B:20 C:10 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 24:    A:10 B:20 C:19 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 25:    A:10 B:19 C:18 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 26:    A:10 B:18 C:11 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 27:    A:15 B:0 C:27 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 28:    A:15 B:27 C:28 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 29:    A:44 B:26 C:16 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 30:    A:44 B:16 C:43 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 31:    A:29 B:14 C:15 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 32:    A:29 B:15 C:28 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 33:    A:30 B:13 C:14 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 34:    A:30 B:14 C:29 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 35:    A:31 B:12 C:13 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 36:    A:31 B:13 C:30 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 37:    A:32 B:11 C:12 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 38:    A:32 B:12 C:31 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 39:    A:10 B:11 C:32 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 40:    A:10 B:32 C:33 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1

                                     Page 3



Face 41:    A:9 B:10 C:33 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 42:    A:9 B:33 C:34 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 43:    A:8 B:9 C:34 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 44:    A:8 B:34 C:35 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 45:    A:7 B:8 C:35 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 46:    A:7 B:35 C:36 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 47:    A:6 B:7 C:36 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 48:    A:6 B:36 C:37 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 49:    A:5 B:6 C:37 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 50:    A:5 B:37 C:38 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 51:    A:4 B:5 C:38 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 52:    A:4 B:38 C:39 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 53:    A:4 B:39 C:3 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 54:    A:3 B:39 C:40 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 55:    A:3 B:40 C:2 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 56:    A:2 B:40 C:41 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 57:    A:2 B:41 C:1 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 58:    A:1 B:41 C:42 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 59:    A:1 B:42 C:0 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 60:    A:0 B:42 C:27 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 61:    A:27 B:42 C:41 AB:1 BC:1 CA:1

                                     Page 4



Material:"r210g210b210a0"
Smoothing:  1
Face 62:    A:27 B:41 C:40 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 63:    A:27 B:40 C:39 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 64:    A:27 B:39 C:44 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 65:    A:27 B:44 C:43 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 66:    A:27 B:43 C:53 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 67:    A:53 B:32 C:31 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 68:    A:53 B:31 C:30 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 69:    A:53 B:30 C:29 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 70:    A:27 B:53 C:28 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 71:    A:53 B:29 C:28 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 72:    A:44 B:39 C:45 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 73:    A:45 B:39 C:38 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 74:    A:45 B:38 C:46 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 75:    A:46 B:38 C:37 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 76:    A:46 B:37 C:47 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 77:    A:47 B:37 C:36 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 78:    A:47 B:36 C:48 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 79:    A:48 B:36 C:35 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 80:    A:48 B:35 C:49 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 81:    A:49 B:35 C:34 AB:1 BC:1 CA:1
Material:"r210g210b210a0"

                                     Page 5



Smoothing:  1
Face 82:    A:49 B:34 C:50 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 83:    A:50 B:34 C:33 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 84:    A:50 B:33 C:51 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 85:    A:51 B:33 C:52 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 86:    A:52 B:33 C:32 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 87:    A:53 B:52 C:32 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 88:    A:45 B:25 C:26 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 89:    A:45 B:26 C:44 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 90:    A:46 B:24 C:25 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 91:    A:46 B:25 C:45 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 92:    A:23 B:24 C:46 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 93:    A:23 B:46 C:47 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 94:    A:22 B:23 C:47 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 95:    A:22 B:47 C:48 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 96:    A:21 B:22 C:48 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 97:    A:21 B:48 C:49 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 98:    A:20 B:21 C:49 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 99:    A:20 B:49 C:50 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 100:    A:20 B:50 C:19 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 101:    A:19 B:50 C:51 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1

                                     Page 6



Face 102:    A:19 B:51 C:18 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 103:    A:18 B:51 C:52 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 104:    A:18 B:52 C:17 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 105:    A:17 B:52 C:53 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 106:    A:16 B:17 C:53 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1
Face 107:    A:16 B:53 C:43 AB:1 BC:1 CA:1
Material:"r210g210b210a0"
Smoothing:  1












































                                     Page 7



