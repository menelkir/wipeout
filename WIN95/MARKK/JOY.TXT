    mov word ptr count, 0

    cli          { Disable interrupts so they don't interfere with timing }
    mov dx, JOYSTICKPORT   { Write dummy byte to joystick port }
    out dx, al
    @joystickloop:
    inc count              { Add one to count }
    cmp count, $FFFF       { Check for time out }
    je @done
    in al, dx              { Get joystick port value }
    and al, axisnum        { Test the appropriate bit }
    jne @joystickloop
    @done:
    sti                    { Enable interrupts again }
  end;
