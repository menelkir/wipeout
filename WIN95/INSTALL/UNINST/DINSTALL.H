/*==========================================================================
 *
 *  Copyright (C) 1995 Microsoft Corporation.  All Rights Reserved.
 *
 *  File:       dinstall.h
 *  Content:    Game SDK sample setup program
 ***************************************************************************/
 

#ifndef __DINSTALL_H__
#define __DINSTALL_H__

#define IDC_EDIT        	1000
#define IDC_EDITTEXT    	1001
#define IDC_STATIC      	1002
#define IDC_REBOOT1     	1003
#define IDC_REBOOT2     	1004
#define IDC_DIRECTIONS1 	1005
#define IDC_DIRECTIONS2 	1006
#define IDC_H           	1007
#define IDC_B           	1008
#define IDC_INFO        	1009
#define IDC_SUCCESS     	1010
#define IDC_BROWSE      	1011


#endif
